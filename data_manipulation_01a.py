INPUT = r'data\raw_levels.csv'


def main():
    with open(INPUT, 'r') as f:
        for _ in range(10):
            next(f)

    print('Finished')


if __name__ == '__main__':
    main()