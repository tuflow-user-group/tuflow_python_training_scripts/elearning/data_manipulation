import pandas as pd
from datetime import datetime
from dateutil.parser import parse

INPUT = r'data\raw_levels.csv'
OUTPUT = r'output\boundary_02.csv'
QC_MAX = 90
START_DATE = datetime(2012, 1, 25)
END_DATE = datetime(2012, 1, 27)


def main():
    print(f'Reading {INPUT}... ')

    df = pd.read_csv(
        INPUT,
        skiprows=9,
        parse_dates=['#Timestamp'],
        date_parser=lambda x: parse(x, ignoretz=True),
        index_col='#Timestamp'
    )
    df = df[df['Quality Code'] <= QC_MAX][START_DATE:END_DATE]
    df['Time'] = (df.index - START_DATE).total_seconds() / 3600
    df[['Time', 'Value']].rename(
        columns={'Time': 'Time (hr):', 'Value': 'Level (m AHD)'}
                                 ).to_csv(OUTPUT, index=False)

    print('Finished')


if __name__ == '__main__':
    main()