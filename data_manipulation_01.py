INPUT = r'data\raw_levels.csv'


def main():
    f = open(INPUT, 'r')
    for i in range(10):
        line = f.readline()
        print(line)

    f.close()
    print('Finished')


if __name__ == '__main__':
    main()