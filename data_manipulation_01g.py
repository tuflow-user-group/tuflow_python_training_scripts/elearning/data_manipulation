from dateutil.parser import parse, ParserError
import matplotlib.pyplot as plt
from datetime import datetime


INPUT = r'data\raw_levels.csv'
OUTPUT = r'output\boundary.csv'
QC_MAX = 90
START_DATE = datetime(2012, 1, 25)
END_DATE = datetime(2012, 1, 27)


def main():
    dates = []
    levels = []
    times = []
    with open(INPUT, 'r') as f:
        line_no = 0
        for _ in range(10):
            next(f)
            line_no += 1

        print(f'Reading file {INPUT}...')
        for line in f:
            line_no += 1
            data = line.split(',')

            # convert date
            try:
                date = parse(data[0], ignoretz=True)
            except ParserError:
                print(f'ERROR: converting date data on line {line_no} - line: {line}')
                continue

            # convert level
            try:
                level = float(data[1])
            except ValueError:
                print(f'ERROR: converting level data on line {line_no} - line: {line}')
                continue

            if len(data) >= 4:
                qc = int(data[3])
                if qc <= QC_MAX:
                    if START_DATE <= date <= END_DATE:
                        dates.append(date)
                        levels.append(level)
                        delta_time = date - START_DATE
                        time = delta_time.total_seconds() / 60. / 60.
                        times.append(time)

    # plt.plot(times, levels, label='BYRON CREEK AT BINNA BURRA')
    # plt.grid()
    # plt.xlabel('Time (hrs)')
    # plt.ylabel('Level (m AHD)')
    # plt.legend()
    # plt.show()

    print(f'Writing out data to {OUTPUT}...')
    with open(OUTPUT, 'w') as f:
        f.write('time (hr),level (m AHD)\n')
        for i, time in enumerate(times):
            level = levels[i]
            f.write(f'{time:.02f},{level:.03f}\n')

    print('Finished')


if __name__ == '__main__':
    main()